-- Pitanje 1
SELECT proizvod.naziv,drzava.kod FROM proizvod JOIN proizvodjac 
ON proizvodjac.proizvodjacID = proizvod.proizvodjacID JOIN grad 
ON proizvodjac.gradID = grad.id JOIN drzava 
ON grad.drzavaID = drzava.kod ORDER BY drzava.kod;
-- Pitanje 2
SELECT COUNT(proizvodID), proizvodjac.naziv,proizvod.naziv 
FROM proizvod JOIN proizvodjac 
ON proizvodjac.proizvodjacID = proizvod.proizvodjacID 
GROUP BY proizvodjac.naziv;
-- Pitanje 3
SELECT prodavnica.naziv,grad.naziv FROM prodavnica JOIN grad 
ON grad.id = prodavnica.gradID WHERE grad.naziv LIKE 'Novi Sad'
-- Pitanje 4
SELECT proizvod.naziv,prodavnica.naziv FROM prodavnica_proizvod JOIN proizvod 
ON prodavnica_proizvod.proizvodID = proizvod.proizvodID JOIN prodavnica 
ON prodavnica.prodavnicaID = prodavnica_proizvod.prodanicaID WHERE prodavnica.naziv LIKE 'Maxi'
-- Pitanje 5
SELECT proizvod.naziv,kategorija.naziv FROM proizvod JOIN kategorija 
ON kategorija.kategorijaID = proizvod.kategorijaID
WHERE kategorija.naziv LIKE 'Pre%';
-- Pitanje 6
SELECT COUNT(proizvodID),kategorija.naziv FROM proizvod JOIN kategorija 
ON kategorija.kategorijaID = proizvod.kategorijaID
GROUP BY kategorija.naziv;
